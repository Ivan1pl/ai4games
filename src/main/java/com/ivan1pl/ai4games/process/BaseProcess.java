/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivan1pl.ai4games.process;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.UUID;

import com.ivan1pl.ai4games.source.BaseProgram;

/**
 *
 * @author Ivan1pl
 */
public abstract class BaseProcess {
    private static final String TMP_DIR = System.getProperty("java.io.tmpdir") + "/ai4games/" + UUID.randomUUID().toString();

    private final BaseProgram program;
    private Process process;

    private final File runDirectory;
    
    private BufferedReader isReader = null;
    private BufferedReader errReader = null;

    public BaseProcess(BaseProgram program) {
        this.program = program;

        this.runDirectory = new File(TMP_DIR, "r" + UUID.randomUUID().toString());
        this.runDirectory.mkdirs();
    }

    public void start(String... args) {
        process = program.start(runDirectory, args);
        if (process != null) {
            isReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            errReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
        }
    }

    public void kill() {
        if (process != null) {
            process.destroy();
            process = null;
        }
    }

    public boolean isRunning() {
        return process != null && process.isAlive();
    }

    public void writeStdin(String input) throws IOException {
        if (process != null && process.isAlive()) {
            OutputStream os = process.getOutputStream();
            os.write((input + "\n").getBytes());
            os.flush();
        }
    }

    public String readStdout() {
        if (process != null && process.isAlive()) {
            try {
                return isReader.readLine();
            } catch (IOException ex) {
                //Stream closed - return null
            }
        }
        return null;
    }

    public String readStderr() {
        if (process != null && process.isAlive()) {
            try {
                return errReader.readLine();
            } catch (IOException ex) {
                //Stream closed - return null
            }
        }
        return null;
    }

    public void pause() {
        //TODO
    }

    public void resume() {
        //TODO
    }

    public int waitFor() throws InterruptedException {
        if (process != null) {
            return process.waitFor();
        }
        return -1;
    }

}

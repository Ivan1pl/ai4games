package com.ivan1pl.ai4games.cli;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.ivan1pl.ai4games.configuration.ContestType;
import com.ivan1pl.ai4games.configuration.ExecutionSettings;

public class ArgumentParser {

    private static final String CMD_LINE_SYNTAX = "java -jar AIContester.jar [options]";
    private static final String DEFAULT_PLAYER_COUNT = "2";
    private static final String DEFAULT_ROUND_COUNT = "0";
    private static final String DEFAULT_PLAY_COUNT = "1";
    private static final String DEFAULT_CONTEST_TYPE = ""; // will be changed to default
    private static final String DEFAULT_PLAYER_LIST = "";

    private final String[] arguments;
    private CommandLineParser parser;
    private Options options;
    private CommandLine cli;

    public ArgumentParser(String[] arguments) throws ParseException {
        this.arguments = arguments;

        initializeOptions();
        initializeParser();
        parse();
    }

    public ExecutionSettings getExecutionSettings() {
        String gameName = cli.getOptionValue("g");
        int playerCount = Integer.parseInt(cli.getOptionValue("p", DEFAULT_PLAYER_COUNT));
        int roundCount = Integer.parseInt(DEFAULT_ROUND_COUNT); // Integer.parseInt(cli.getOptionValue("r", DEFAULT_ROUND_COUNT));
        int playCount = Integer.parseInt(cli.getOptionValue("n", DEFAULT_PLAY_COUNT));
        String playerList = cli.getOptionValue("l", DEFAULT_PLAYER_LIST);
        ContestType contestType = ContestType.parse(cli.getOptionValue("s", DEFAULT_CONTEST_TYPE));
        boolean isHelp = cli.hasOption("h");
        String replay = cli.getOptionValue("R");
        String replayPlayer = cli.getOptionValue("P");

        List<String> players = parsePlayerList(playerList);

        return new ExecutionSettings(gameName, playerCount, roundCount, playCount, contestType, players, isHelp, replay, replayPlayer);
    }

    private void initializeOptions() {
        this.options = new Options();

        options.addOption("g", "game", true, "game name");
        options.addOption("s", "system", true, "queue system ('league', 'turnament'" + /* ", 'swiss'" */")");
        options.addOption("p", "players", true, "player count (default: 2)");
        options.addOption("l", "player-list", true, "comma separated list of participating players (default: all players)");
        // options.addOption("r", "rounds", true, "round count (default: game's default value)");
        options.addOption("n", "plays", true, "play count/repeats (default: 1)");
        options.addOption("h", "help", false, "display this help and quit");
        options.addOption("R", "replay", true, "path to replay; if set, all parameters except 'P' and 'g' are ignored");
        options.addOption("P", "replay-player", true,
                "player for whom the replay is played (debug informations will be displayed only for this player; if not set, debug informations will be displayed to all players)");
    }

    private void initializeParser() {
        this.parser = new DefaultParser();
    }

    private void parse() throws ParseException {
        try {
            this.cli = parser.parse(options, arguments);

            if (!cli.hasOption("g")) {
                throw new ParseException("Didn't provide argument: 'g' (game's name)");
            }
        } catch(ParseException exception) {
            printHelp();
            throw exception;
        }
    }

    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(CMD_LINE_SYNTAX, options);
    }

    private static List<String> parsePlayerList(String playerList) {
        List<String> result = null;
        if (playerList != null && playerList.length() > 0) {
            String[] players = playerList.split(",");
            result = Arrays.asList(players).stream().distinct().collect(Collectors.toList());
        }
        return result;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivan1pl.ai4games.source;

import java.io.File;

/**
 *
 * @author Ivan1pl
 */
public abstract class CompiledProgram extends SourceProgram {
    
    private File executable;

    public CompiledProgram(File source) {
        super(source);
    }

    @Override
    public Process start(File runDir, String... args) {
        if (executable == null) {
            executable = findExecutable();
        }
        if (executable == null || executable.lastModified() < getSource().lastModified()) {
            executable = compile();
        }
        return run(runDir, args);
    }
    
    protected File getExecutable() {
        return executable;
    }
    
    protected abstract File findExecutable();
    
    protected abstract File compile();
    
    protected abstract Process run(File runDir, String... args);
    
}

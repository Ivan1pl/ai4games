/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivan1pl.ai4games.source;

import java.io.File;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author Ivan1pl
 */
public abstract class BaseProgram {

    public abstract Process start(File runDir, String... args);

    protected static String[] packArgs(String[] additionalArgs, String... args) {
        return ArrayUtils.addAll(args, additionalArgs);
    }

}

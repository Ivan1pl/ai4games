/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivan1pl.ai4games.source;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FilenameUtils;

/**
 *
 * @author Ivan1pl
 */
public class JavaProgram extends CompiledProgram {

    public JavaProgram(File source) {
        super(source);
    }

    @Override
    protected File findExecutable() {
        File source = getSource();
        File sourceDir = source.getParentFile();
        String targetName = FilenameUtils.removeExtension(source.getName()) + ".class";
        for (File f : sourceDir.listFiles()) {
            if (targetName.equals(f.getName())) {
                return f;
            }
        }
        return null;
    }

    @Override
    protected File compile() {
        try {
            File source = getSource();
            File parentDir = source.getParentFile();
            File outputLog = new File(parentDir, "compilation.out");
            File errorLog = new File(parentDir, "compilation.err");
            String classPath = new File("").getAbsolutePath() + File.separator + "*";
            int exitCode = new ProcessBuilder("javac", "-cp", classPath, source.getAbsolutePath())
                    .directory(parentDir)
                    .redirectOutput(outputLog)
                    .redirectError(errorLog)
                    .start()
                    .waitFor();
        } catch (IOException | InterruptedException ex) {
            return null;
        }
        return findExecutable();
    }

    @Override
    protected Process run(File runDir, String... args) {
        File executable = getExecutable();
        if (executable == null) {
            return null;
        }
        try {
            String targetName = FilenameUtils.removeExtension(executable.getName());
            File parentDir = executable.getParentFile();
            String classPath = new File(".").getAbsolutePath() + File.separator + "*" + File.pathSeparator + parentDir.getAbsolutePath();
            return new ProcessBuilder(packArgs(args, "java", "-classpath", classPath, targetName))
                    .directory(runDir)
                    .start();
        } catch (IOException ex) {
            return null;
        }
    }

}

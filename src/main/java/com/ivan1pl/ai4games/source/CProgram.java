/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivan1pl.ai4games.source;

import java.io.File;
import java.io.IOException;

/**
 *
 * @author Ivan1pl
 */
public class CProgram extends CompiledProgram {

    public CProgram(File source) {
        super(source);
    }

    @Override
    protected File findExecutable() {
        File source = getSource();
        File sourceDir = source.getParentFile();
        for (File f : sourceDir.listFiles()) {
            if ("main".equals(f.getName())) {
                return f;
            }
        }
        return null;
    }

    @Override
    protected File compile() {
        try {
            File source = getSource();
            File parentDir = source.getParentFile();
            File outputLog = new File(parentDir, "compilation.out");
            File errorLog = new File(parentDir, "compilation.err");
            int exitCode = new ProcessBuilder("gcc", "-o", "main", source.getAbsolutePath())
                    .directory(parentDir)
                    .redirectOutput(outputLog)
                    .redirectError(errorLog)
                    .start()
                    .waitFor();
        } catch (IOException | InterruptedException ex) {
            return null;
        }
        return findExecutable();
    }

    @Override
    protected Process run(File runDir, String... args) {
        File executable = getExecutable();
        if (executable == null) {
            return null;
        }
        try {
            return new ProcessBuilder(packArgs(args, executable.getAbsolutePath()))
                    .directory(runDir)
                    .start();
        } catch (IOException ex) {
            return null;
        }
    }
    
}

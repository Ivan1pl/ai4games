/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivan1pl.ai4games.source;

import java.io.File;
import org.apache.commons.io.FilenameUtils;

/**
 *
 * @author Ivan1pl
 */
public class JavaExecutableProgram extends ExecutableProgram {
    private final boolean isJar;

    public JavaExecutableProgram(File executable, boolean isJar) {
        super(executable, prepareWrapperCommands(executable, isJar));
        this.isJar = isJar;
    }

    private static String[] prepareWrapperCommands(File exec, boolean jar) {
        File parentDir = exec.getParentFile();
        String classPath = new File(".").getAbsolutePath() + File.separator + "*" + File.pathSeparator + parentDir.getAbsolutePath();
        if (jar) {
            return new String[] {"java", "-classpath", classPath, "-jar"};
        } else {
            return new String[] {"java", "-classpath", classPath};
        }
    }

    @Override
    protected String[] wrapCommand(File executable) {
        if (isJar) {
            return super.wrapCommand(executable);
        } else {
            return super.wrapCommand(FilenameUtils.removeExtension(executable.getName()));
        }
    }

}

package com.ivan1pl.ai4games.source;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class ExecutableProgram extends BaseProgram {
    
    private final File executable;
    private final String[] wrapperCommands;
    
    public ExecutableProgram(File executable, String... wrapperCommands) {
        this.executable = executable;
        this.wrapperCommands = wrapperCommands;
    }
    
    @Override
    public Process start(File runDir, String... args) {
        if (executable == null) {
            return null;
        }
        
        try {
            return new ProcessBuilder(packArgs(args, wrapCommand(executable)))
                    .directory(runDir)
                    .start();
        } catch (IOException ex) {
            return null;
        }
    }

    protected String[] wrapCommand(String command) {
        String[] processArgs = Arrays.copyOf(wrapperCommands, wrapperCommands.length + 1);
        processArgs[processArgs.length-1] = command;
        return processArgs;
    }

    protected String[] wrapCommand(File executable) {
        return wrapCommand(executable.getAbsolutePath());
    }
    
}

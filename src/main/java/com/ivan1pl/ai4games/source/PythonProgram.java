/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivan1pl.ai4games.source;

import java.io.File;
import java.io.IOException;

/**
 *
 * @author Ivan1pl
 */
public class PythonProgram extends InterpretedProgram {

    public PythonProgram(File source) {
        super(source);
    }

    @Override
    public Process start(File runDir, String... args) {
        try {
            return new ProcessBuilder(packArgs(args, "python", getSource().getAbsolutePath()))
                    .directory(runDir)
                    .start();
        } catch (IOException ex) {
            return null;
        }
    }
    
}

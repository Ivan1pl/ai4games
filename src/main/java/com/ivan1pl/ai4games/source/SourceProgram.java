package com.ivan1pl.ai4games.source;

import java.io.File;

public abstract class SourceProgram extends BaseProgram {
    
    private final File source;
    
    public SourceProgram(File source) {
        this.source = source;
    }
    
    protected File getSource() {
        return source;
    }
    
}

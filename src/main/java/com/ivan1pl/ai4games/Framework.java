/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivan1pl.ai4games;

import java.security.InvalidParameterException;

import org.apache.commons.cli.ParseException;

import com.ivan1pl.ai4games.cli.ArgumentParser;
import com.ivan1pl.ai4games.configuration.ExecutionSettings;
import com.ivan1pl.ai4games.entity.DefaultGameFactory;
import com.ivan1pl.ai4games.entity.Game;
import com.ivan1pl.ai4games.entity.GameFactory;
import com.ivan1pl.ai4games.exceptions.InvalidPlayerException;
import com.ivan1pl.ai4games.exceptions.NotEnoughPlayersException;
import com.ivan1pl.ai4games.runner.ContestRunner;
import com.ivan1pl.ai4games.runner.ContestRunnerSelector;
import com.ivan1pl.ai4games.stats.ContestStatistics;

/**
 *
 * @author Ivan1pl
 */
public class Framework {

    private static final int EXIT_SUCCESS = 0;
    private static final int EXIT_FAILURE = -1;

    public static void main(String[] args) {
        try {
            ArgumentParser argumentParser = new ArgumentParser(args);
            ExecutionSettings executionSettings = argumentParser.getExecutionSettings();

            selectPath(executionSettings, argumentParser);

        } catch (ParseException exception) {
            System.err.println("Failed to parse arguments!");
            System.err.println(exception.getMessage());
            System.exit(EXIT_FAILURE);

        } catch (InvalidParameterException exception) {
            System.err.println("Provided invalid parameters!");
            System.err.println(exception.getMessage());
            System.exit(EXIT_FAILURE);

        } catch (InvalidPlayerException exception) {
            System.err.println("Invalid player names!");
            System.err.println(exception.getMessage());
            System.exit(EXIT_FAILURE);

        } catch(Exception exception) {
            System.err.println(exception.getMessage());
            System.exit(EXIT_FAILURE);
        }

        System.exit(EXIT_SUCCESS);
    }

    private static void selectPath(ExecutionSettings settings, ArgumentParser argumentParser)
            throws InvalidParameterException, InvalidPlayerException, NotEnoughPlayersException {
        if (settings.isHelp) {
            selectHelpPath(argumentParser);
        } else if (settings.replay != null) {
            selectReplayPath(settings);
        } else {
            selectGamePath(settings);
        }

        System.exit(EXIT_SUCCESS);
    }

    private static void selectHelpPath(ArgumentParser argumentParser) {
        argumentParser.printHelp();
    }

    private static void selectReplayPath(ExecutionSettings settings) {
        GameFactory gameFactory = new DefaultGameFactory(settings);
        Game game = gameFactory.getGame();
        if (game != null && game.process != null) {
            if (settings.replayPlayer == null) {
                game.process.start(settings.replay);
            } else {
                game.process.start(settings.replay, settings.replayPlayer);
            }
            try {
                game.process.waitFor();
            } catch (InterruptedException ex) {
                System.err.println(ex.getMessage());
                System.exit(EXIT_FAILURE);
            }
        }
    }

    private static void selectGamePath(ExecutionSettings settings)
            throws InvalidParameterException, InvalidPlayerException, NotEnoughPlayersException {
        GameFactory gameFactory = new DefaultGameFactory(settings);
        ContestRunnerSelector selector = new ContestRunnerSelector(gameFactory);
        ContestRunner runner = selector.getContestRunner(settings);
        ContestStatistics statistics = runner.run();
        statistics.print();
    }

}

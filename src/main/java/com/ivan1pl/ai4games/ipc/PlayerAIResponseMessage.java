package com.ivan1pl.ai4games.ipc;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.ivan1pl.ai4games.entity.PlayerAI;
import com.ivan1pl.ai4games.entity.PlayerAIStatus;

public class PlayerAIResponseMessage extends BaseMessage {

    public String playerOutput;
    public String playerDebugOutput;

    public PlayerAIResponseMessage() {

    }

    public PlayerAIResponseMessage(GameResponseMessage gameMessage) {
        this.currentPlayer = gameMessage.currentPlayer;
        this.extraData = gameMessage.extraData;
        this.players = gameMessage.players;
        this.playerOutput = "";
        this.playerDebugOutput = "";
    }

    public static PlayerAIResponseMessage deserialize(String json) {
        return new Gson().fromJson(json, PlayerAIResponseMessage.class);
    }

    public static PlayerAIResponseMessage getInitialMessage(List<PlayerAI> players) {
        PlayerAIResponseMessage initialMessage = new PlayerAIResponseMessage();

        initialMessage.currentPlayer = "";
        initialMessage.extraData = "INITIALIZATION";
        initialMessage.playerOutput = "";
        initialMessage.playerDebugOutput = "";
        initialMessage.players = getInitialPlayerStatusList(players);

        return initialMessage;
    }

    private static List<PlayerAIStatusField> getInitialPlayerStatusList(List<PlayerAI> players) {
        List<PlayerAIStatusField> initialPlayerStatusList = new ArrayList<>();

        for (PlayerAI player : players) {
            PlayerAIStatusField playerStatus = new PlayerAIStatusField();
            playerStatus.id = player.id;
            playerStatus.status = PlayerAIStatus.ALIVE;

            initialPlayerStatusList.add(playerStatus);
        }

        return initialPlayerStatusList;
    }

}

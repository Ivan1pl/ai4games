package com.ivan1pl.ai4games.ipc;

import java.util.List;

import com.google.gson.Gson;

public abstract class BaseMessage {
    
    public List<PlayerAIStatusField> players = null;
    public String currentPlayer = null;
    public String extraData = null;
    
    public String serialize() {
        return new Gson().toJson(this);
    }
}

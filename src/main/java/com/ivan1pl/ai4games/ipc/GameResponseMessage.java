package com.ivan1pl.ai4games.ipc;

import com.google.gson.Gson;

public class GameResponseMessage extends BaseMessage {

    public String playerInput;
    
    public static GameResponseMessage deserialize(String json) {
        return new Gson().fromJson(json, GameResponseMessage.class);
    }

}

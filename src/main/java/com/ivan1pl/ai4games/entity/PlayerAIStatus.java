package com.ivan1pl.ai4games.entity;

public enum PlayerAIStatus {
    
    ALIVE,
    WON,
    LOST,
    TIE,
    OUT;
    
}

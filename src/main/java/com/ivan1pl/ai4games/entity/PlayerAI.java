package com.ivan1pl.ai4games.entity;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ivan1pl.ai4games.ipc.GameResponseMessage;
import com.ivan1pl.ai4games.ipc.PlayerAIResponseMessage;
import com.ivan1pl.ai4games.ipc.PlayerAIStatusField;
import com.ivan1pl.ai4games.process.PlayerProcess;
import com.ivan1pl.ai4games.runner.GameRunner;
import com.ivan1pl.ai4games.source.BaseProgram;

public class PlayerAI {

    private static final int STDERR_TIMEOUT = 1;

    public final String id;
    public final PlayerProcess process;

    private final ExecutorService readExecutor;
    private final ExecutorService debugReadExecutor;
    private Future<String> debugReadTask;

    public PlayerAI(String id, BaseProgram program) {
        this.id = id;
        this.process = new PlayerProcess(program);
        this.readExecutor = Executors.newSingleThreadExecutor();

        this.debugReadTask = null;
        this.debugReadExecutor = Executors.newSingleThreadExecutor();

    }

    public PlayerAIResponseMessage readMessage(int timeout, GameResponseMessage gameMessage) {
        PlayerAIResponseMessage responseMessage = new PlayerAIResponseMessage(gameMessage);

        // Read data with timeout
        {
            Callable<String> readTask = new Callable<String>() {
                @Override
                public String call() throws Exception {
                    // This reads only first line, but in future it might be
                    // extended to read specific number of lines
                    return process.readStdout();
                }
            };

            Future<String> future = readExecutor.submit(readTask);

            try {
                String playerResponse = future.get(timeout, TimeUnit.MILLISECONDS);
                responseMessage.playerOutput = playerResponse;

                // If player responded, then try to read his debug message
                responseMessage.playerDebugOutput = readDebugMessage();

            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                Logger.getLogger(GameRunner.class.getName()).log(Level.WARNING,
                        "Player " + id + " did not respond in given time (" + timeout + "ms)!");

                for (PlayerAIStatusField playerStatus : responseMessage.players) {
                    if (playerStatus.id.equals(id)) {
                        playerStatus.status = PlayerAIStatus.OUT;
                    }
                }
            }
        }

        return responseMessage;
    }

    private String readDebugMessage() {
        String debugMessage = "";

        try {
            // Only create new task if previous one already finiesh and response was processed
            if (debugReadTask == null) {
                Callable<String> debugReadFunc = new Callable<String>() {
                    @Override
                    public String call() throws Exception {
                        // This reads only first line, but in future it
                        // might be extended to read specific number of lines
                        return process.readStderr();
                    }
                };
                debugReadTask = debugReadExecutor.submit(debugReadFunc);
            }

            debugMessage = debugReadTask.get(STDERR_TIMEOUT, TimeUnit.MILLISECONDS);
            debugReadTask = null;

        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            // Ignore if he didn't sent anything
        }

        return debugMessage;
    }

    public void sendMessage(GameResponseMessage gameMessage) {
        try {
            process.writeStdin(gameMessage.playerInput);
        } catch (IOException e) {
            Logger.getLogger(GameRunner.class.getName()).log(Level.WARNING,
                    "Could not send message to player: " + id, e);
        }
    }

}

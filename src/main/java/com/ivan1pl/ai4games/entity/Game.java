package com.ivan1pl.ai4games.entity;

import java.io.IOException;
import java.security.InvalidParameterException;

import com.ivan1pl.ai4games.configuration.ExecutionSettings;
import com.ivan1pl.ai4games.configuration.GameConfig;
import com.ivan1pl.ai4games.ipc.GameResponseMessage;
import com.ivan1pl.ai4games.ipc.PlayerAIResponseMessage;
import com.ivan1pl.ai4games.loggers.Logger;
import com.ivan1pl.ai4games.process.GameProcess;
import com.ivan1pl.ai4games.source.BaseProgram;

public class Game {

    public final String id;
    public final int players;
    public final GameProcess process;
    public final GameConfig config;

    public Game(ExecutionSettings settings, BaseProgram gameProgram, GameConfig config) throws InvalidParameterException {
        this.id = settings.gameName;
        this.players = settings.playerCount;
        this.process = new GameProcess(gameProgram);
        this.config = config;

        validatePlayerCount();
    }

    private void validatePlayerCount() throws InvalidParameterException {
        if (players < config.minPlayers || players > config.maxPlayers) {
            String exceptionMsg = "Invalid playerCount argument!\n";
            exceptionMsg += "Given: " + players + "\n";
            exceptionMsg += "Supported range for game " + id + ": [" + config.minPlayers + ", " + config.maxPlayers + "]\n";

            throw new InvalidParameterException(exceptionMsg);
        }
    }

    public GameResponseMessage readMessage(Logger gameplayLogger) throws IOException {
        String gameResponse = process.readStdout();

        if (gameplayLogger != null) {
            gameplayLogger.log(gameResponse);
        } else {
            throw new IOException("Can't read data from game's process!");
        }

        return GameResponseMessage.deserialize(gameResponse);
    }

    public void sendMessage(PlayerAIResponseMessage playerMessage, Logger gameplayLogger) throws IOException {
        String message = playerMessage.serialize();

        if (gameplayLogger != null) {
            gameplayLogger.log(message);
        }

        try {
            process.writeStdin(message);
        } catch (IOException exception) {
            throw new IOException("Can't send data to game's process!", exception);
        }
    }

}

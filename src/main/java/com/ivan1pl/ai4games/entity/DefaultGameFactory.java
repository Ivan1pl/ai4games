package com.ivan1pl.ai4games.entity;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.io.filefilter.DirectoryFileFilter;

import com.ivan1pl.ai4games.configuration.ExecutionSettings;
import com.ivan1pl.ai4games.configuration.GameConfig;
import com.ivan1pl.ai4games.configuration.yaml.YamlConfiguration;
import com.ivan1pl.ai4games.exceptions.InvalidPlayerException;
import com.ivan1pl.ai4games.exceptions.NotEnoughPlayersException;
import com.ivan1pl.ai4games.source.BaseProgram;
import com.ivan1pl.ai4games.source.CProgram;
import com.ivan1pl.ai4games.source.CppProgram;
import com.ivan1pl.ai4games.source.ExecutableProgram;
import com.ivan1pl.ai4games.source.JavaExecutableProgram;
import com.ivan1pl.ai4games.source.JavaProgram;
import com.ivan1pl.ai4games.source.OcamlProgram;
import com.ivan1pl.ai4games.source.PythonProgram;

public class DefaultGameFactory implements GameFactory {

    ExecutionSettings settings;

    public DefaultGameFactory(ExecutionSettings settings) {
        this.settings = settings;
    }

    @Override
    public Game getGame() throws InvalidParameterException {
        String gameDir = "games" + File.separator + settings.gameName + File.separator;
        String gameConfigPath = gameDir + "config.yml";

        File gameConfigFile = new File(gameConfigPath);
        if (!gameConfigFile.exists()) {
            throw new InvalidParameterException("Cannot load game's config file in path: " + gameConfigPath);
        }

        YamlConfiguration gameConfig = new YamlConfiguration(gameConfigFile);

        return new Game(settings, getGameProgram(gameDir, gameConfig), getGameConfig(gameConfig));
    }

    private BaseProgram getGameProgram(String gameDir, YamlConfiguration gameConfig) throws InvalidParameterException {
        BaseProgram result = null;

        String language = gameConfig.getString("execution.language");
        String filePath = gameConfig.getString("execution.path");
        boolean source = gameConfig.getBoolean("execution.source");

        String fullGamePath = gameDir + filePath;
        File file = new File(fullGamePath);

        if (!file.exists()) {
            throw new InvalidParameterException("Cannot load game's file in path: " + fullGamePath);
        }

        if (source) {
            switch (language) {
            case "c":
                result = new CProgram(file);
                break;
            case "cpp":
                result = new CppProgram(file);
                break;
            case "java":
                result = new JavaProgram(file);
                break;
            case "ocaml":
                result = new OcamlProgram(file);
                break;
            case "python":
                result = new PythonProgram(file);
                break;
            default:
                throw new InvalidParameterException("Cannot load game with language: '" + language + "'");
            }
        } else {
            switch (language) {
            case "c":
            case "cpp":
            case "exec":
                result = new ExecutableProgram(file);
                break;
            case "java":
                result = new JavaExecutableProgram(file, false);
                break;
            case "jar":
                result = new JavaExecutableProgram(file, true);
                break;
            case "ocaml":
                result = new ExecutableProgram(file, "ocamlrun");
                break;
            case "python":
                result = new PythonProgram(file);
                break;
            default:
                throw new InvalidParameterException("Cannot load game with language: '" + language + "'");
            }
        }

        return result;
    }

    private static GameConfig getGameConfig(YamlConfiguration gameConfig) {
        int timeout = gameConfig.getInt("gameplay.timeout");
        int minPlayers = gameConfig.getInt("gameplay.minPlayers");
        int maxPlayers = gameConfig.getInt("gameplay.maxPlayers");

        return new GameConfig(timeout, minPlayers, maxPlayers);
    }

    @Override
    public List<PlayerAI> getPlayers() throws InvalidPlayerException, NotEnoughPlayersException {
        List<PlayerAI> players = null;

        String playersDir = "games" + File.separator + settings.gameName + File.separator + "players" + File.separator;
        File gamePlayerPaths = new File(playersDir);
        File[] gamePlayerDirs = gamePlayerPaths.listFiles((FileFilter) DirectoryFileFilter.INSTANCE);
        List<String> playerNames = settings.playerNames;

        if (gamePlayerDirs != null) {
            gamePlayerDirs = Arrays.stream(gamePlayerDirs)
                    .filter(f -> playerNames == null ? true : playerNames.contains(f.getName()))
                    .sorted((f1, f2) -> f1.getName().compareTo(f2.getName()))
                    .toArray(size -> new File[size]);

            if (playerNames != null && playerNames.size() != gamePlayerDirs.length) {
                List<String> existing = Arrays.stream(gamePlayerDirs).map(f -> f.getName()).collect(Collectors.toList());
                List<String> missing = playerNames.stream().filter(n -> !existing.contains(n)).collect(Collectors.toList());
                throw new InvalidPlayerException(getMissingPlayersMessage(missing));
            }

            players = new ArrayList<PlayerAI>();

            for (File playerDir : gamePlayerDirs) {
                // Let's ignore player dirs starting with '.' for on/off switch.
                if (playerDir.getName().startsWith(".")) {
                    continue;
                }

                PlayerAI player = getPlayerFromDir(playerDir);
                if (player != null) {
                    players.add(player);
                }
            }
        }

        if (gamePlayerDirs.length < settings.playerCount) {
            throw new NotEnoughPlayersException(gamePlayerDirs.length, settings.playerCount);
        }

        return players;
    }

    private PlayerAI getPlayerFromDir(File playerDir) {
        PlayerAI player = null;
        String id = playerDir.getName();

        File[] playerSourceFiles = playerDir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String fileName) {
                return fileName.endsWith(".cpp") || fileName.endsWith(".c") || fileName.endsWith(".java")
                        || fileName.endsWith(".ml") || fileName.endsWith(".py");
            }
        });

        if (playerSourceFiles != null && playerSourceFiles.length == 1) {
            File playerSourceFile = playerSourceFiles[0];

            if (playerSourceFile.getName().endsWith(".cpp")) {
                player = new PlayerAI(id, new CppProgram(playerSourceFile));
            } else if (playerSourceFile.getName().endsWith(".c")) {
                player = new PlayerAI(id, new CProgram(playerSourceFile));
            } else if (playerSourceFile.getName().endsWith(".java")) {
                player = new PlayerAI(id, new JavaProgram(playerSourceFile));
            } else if (playerSourceFile.getName().endsWith(".ml")) {
                player = new PlayerAI(id, new OcamlProgram(playerSourceFile));
            } else if (playerSourceFile.getName().endsWith(".py")) {
                player = new PlayerAI(id, new PythonProgram(playerSourceFile));
            }
        }

        return player;
    }

    private static String getMissingPlayersMessage(List<String> missing) {
        return missing.stream().reduce("", (acc, name) -> acc + (acc.length() == 0 ? "" : ", ") + name);
    }

}

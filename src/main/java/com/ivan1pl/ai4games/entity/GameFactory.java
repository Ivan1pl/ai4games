package com.ivan1pl.ai4games.entity;

import java.util.List;

import com.ivan1pl.ai4games.exceptions.InvalidPlayerException;
import com.ivan1pl.ai4games.exceptions.NotEnoughPlayersException;

public interface GameFactory {

    Game getGame();

    List<PlayerAI> getPlayers() throws InvalidPlayerException, NotEnoughPlayersException;

}

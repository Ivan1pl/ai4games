package com.ivan1pl.ai4games.configuration;

public class GameConfig {

    public final int timeout;
    public final int minPlayers;
    public final int maxPlayers;

    public GameConfig(int timeout, int minPlayers, int maxPlayers) {
        this.timeout = timeout;
        this.minPlayers = minPlayers;
        this.maxPlayers = maxPlayers;
    }

}

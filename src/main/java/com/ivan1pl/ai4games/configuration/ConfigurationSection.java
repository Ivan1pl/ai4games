/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivan1pl.ai4games.configuration;

import java.util.List;

/**
 *
 * @author Ivan1pl
 */
public interface ConfigurationSection {
    boolean contains(String path);
    Object get(String path);
    boolean getBoolean(String path);
    double getDouble(String path);
    int getInt(String path);
    long getLong(String path);
    String getString(String path);
    List<Boolean> getBooleanList(String path);
    List<Byte> getByteList(String path);
    List<Character> getCharacterList(String path);
    List<Double> getDoubleList(String path);
    List<Float> getFloatList(String path);
    List<Integer> getIntegerList(String path);
    List<Long> getLongList(String path);
    List<Short> getShortList(String path);
    List<String> getStringList(String path);
    ConfigurationSection getConfigurationSection(String path);
    String getCurrentPath();
}

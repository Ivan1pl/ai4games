/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivan1pl.ai4games.configuration.yaml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.yaml.snakeyaml.Yaml;

import com.ivan1pl.ai4games.configuration.MemorySection;

/**
 *
 * @author Ivan1pl
 */
public class YamlConfiguration extends MemorySection {
    
    private final Yaml yaml = new Yaml();
    
    public YamlConfiguration(File configFile) {
        Map<?,?> mappings = new HashMap<>();
        try {
            mappings = (Map<?,?>) yaml.load(new FileReader(configFile));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(YamlConfiguration.class.getName()).log(Level.SEVERE, null, ex);
        }
        convertMapToSections(mappings);
    }
    
}

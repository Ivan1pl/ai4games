package com.ivan1pl.ai4games.configuration;

import java.util.List;

public class ExecutionSettings {
    public final String gameName;
    public final int playerCount;
    public final int roundCount; // not used for now
    public final int playCount;
    public final ContestType contestType;
    public final List<String> playerNames;
    public final boolean isHelp;
    public final String replay;
    public final String replayPlayer;

    public ExecutionSettings(String gameName, int playerCount, int roundCount, int playCount, ContestType contestType, List<String> playerNames, boolean isHelp, String replay, String replayPlayer) {
        this.gameName = gameName;
        this.playerCount = playerCount;
        this.roundCount = roundCount;
        this.playCount = playCount;
        this.contestType = contestType;
        this.playerNames = playerNames;
        this.isHelp = isHelp;
        this.replay = replay;
        this.replayPlayer = replayPlayer;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivan1pl.ai4games.configuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Ivan1pl
 */
public class MemorySection implements ConfigurationSection {
    
    private static final String PATH_SEPARATOR = ".";
    private static final String ESCAPED_PATH_SEPARATOR = "\\.";
    
    private final Map<String, Object> map = new HashMap<>();
    private final String path;
    private final String fullPath;
    
    // Root section
    protected MemorySection() {
        this.path = "";
        this.fullPath = "";
    }
    
    protected MemorySection(ConfigurationSection parent, String path) {
        this.fullPath = parent.getCurrentPath() + PATH_SEPARATOR + path;
        this.path = path;
    }

    @Override
    public boolean contains(String path) {
        return get(path) != null;
    }

    @Override
    public Object get(String path) {
        if (path.length() == 0) {
            return this;
        }
        String[] parts = path.split(ESCAPED_PATH_SEPARATOR);
        String key = parts[parts.length - 1];
        ConfigurationSection section = this;
        for (int i = 0; i < parts.length - 1; ++i) {
            String s = parts[i];
            section = section.getConfigurationSection(s);
            if (section == null) {
                return null;
            }
        }
        
        if (section == this) {
            return map.get(key);
        }
        
        return section.get(key);
    }

    @Override
    public boolean getBoolean(String path) {
        Object val = get(path);
        return val instanceof Boolean ? (Boolean) val : false;
    }

    @Override
    public double getDouble(String path) {
        Object val = get(path);
        return val instanceof Number ? ((Number) val).doubleValue() : 0.0;
    }

    @Override
    public int getInt(String path) {
        Object val = get(path);
        return val instanceof Number ? ((Number) val).intValue() : 0;
    }

    @Override
    public long getLong(String path) {
        Object val = get(path);
        return val instanceof Number ? ((Number) val).longValue() : 0L;
    }

    @Override
    public String getString(String path) {
        Object val = get(path);
        return val == null ? null : val.toString();
    }
    
    private List<?> getList(String path) {
        Object val = get(path);
        return val instanceof List ? (List<?>) val : null;
    }

    @Override
    public List<Boolean> getBooleanList(String path) {
        List<?> val = getList(path);
        if (val == null) {
            return new ArrayList<>(0);
        }
        
        List<Boolean> result = new ArrayList<>();
        for (Object o : val) {
            if (o instanceof Boolean) {
                result.add((Boolean) o);
            }
        }
        return result;
    }

    @Override
    public List<Byte> getByteList(String path) {
        List<?> val = getList(path);
        if (val == null) {
            return new ArrayList<>(0);
        }
        
        List<Byte> result = new ArrayList<>();
        for (Object o : val) {
            if (o instanceof Number) {
                result.add(((Number) o).byteValue());
            }
        }
        return result;
    }

    @Override
    public List<Character> getCharacterList(String path) {
        List<?> val = getList(path);
        if (val == null) {
            return new ArrayList<>(0);
        }
        
        List<Character> result = new ArrayList<>();
        for (Object o : val) {
            if (o instanceof Character) {
                result.add((Character) o);
            }
        }
        return result;
    }

    @Override
    public List<Double> getDoubleList(String path) {
        List<?> val = getList(path);
        if (val == null) {
            return new ArrayList<>(0);
        }
        
        List<Double> result = new ArrayList<>();
        for (Object o : val) {
            if (o instanceof Number) {
                result.add(((Number) o).doubleValue());
            }
        }
        return result;
    }

    @Override
    public List<Float> getFloatList(String path) {
        List<?> val = getList(path);
        if (val == null) {
            return new ArrayList<>(0);
        }
        
        List<Float> result = new ArrayList<>();
        for (Object o : val) {
            if (o instanceof Number) {
                result.add(((Number) o).floatValue());
            }
        }
        return result;
    }

    @Override
    public List<Integer> getIntegerList(String path) {
        List<?> val = getList(path);
        if (val == null) {
            return new ArrayList<>(0);
        }
        
        List<Integer> result = new ArrayList<>();
        for (Object o : val) {
            if (o instanceof Number) {
                result.add(((Number) o).intValue());
            }
        }
        return result;
    }

    @Override
    public List<Long> getLongList(String path) {
        List<?> val = getList(path);
        if (val == null) {
            return new ArrayList<>(0);
        }
        
        List<Long> result = new ArrayList<>();
        for (Object o : val) {
            if (o instanceof Number) {
                result.add(((Number) o).longValue());
            }
        }
        return result;
    }

    @Override
    public List<Short> getShortList(String path) {
        List<?> val = getList(path);
        if (val == null) {
            return new ArrayList<>(0);
        }
        
        List<Short> result = new ArrayList<>();
        for (Object o : val) {
            if (o instanceof Number) {
                result.add(((Number) o).shortValue());
            }
        }
        return result;
    }

    @Override
    public List<String> getStringList(String path) {
        List<?> val = getList(path);
        if (val == null) {
            return new ArrayList<>(0);
        }
        
        List<String> result = new ArrayList<>();
        for (Object o : val) {
            if (o instanceof String) {
                result.add(o.toString());
            }
        }
        return result;
    }

    @Override
    public ConfigurationSection getConfigurationSection(String path) {
        Object val = get(path);
        return val instanceof ConfigurationSection ? (ConfigurationSection) val : null;
    }

    @Override
    public String getCurrentPath() {
        return fullPath;
    }
    
    protected final void convertMapToSections(Map<?,?> mappings) {
        map.clear();
        for (Map.Entry<?,?> entry : mappings.entrySet()) {
            String key = entry.getKey().toString();
            Object value = entry.getValue();
            
            if (value instanceof Map) {
                MemorySection childSection = new MemorySection(this, key);
                childSection.convertMapToSections((Map<?,?>) value);
                map.put(key, childSection);
            } else {
                map.put(key, value);
            }
        }
    }
    
}

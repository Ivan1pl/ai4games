package com.ivan1pl.ai4games.configuration;

import java.util.logging.Level;
import java.util.logging.Logger;

public enum ContestType {
    SINGLE_ELIMINATION,
    ROUND_ROBIN,
    SWISS;

    private static ContestType DEFAULT_TYPE = ROUND_ROBIN;

    public static ContestType parse(String contestType) {
         switch (contestType) {
            case "tournament":
                return SINGLE_ELIMINATION;
            case "league":
                return ROUND_ROBIN;
            /*
            case "swiss":
                return SWISS;
            */
            default:
                if (!contestType.isEmpty()) {
                    Logger.getLogger(ContestType.class.getName()).log(Level.WARNING, "Invalid contestType: '" + contestType + "'");
                }
                return DEFAULT_TYPE;
        }
    }
}

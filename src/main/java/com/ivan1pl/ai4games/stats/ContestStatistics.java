package com.ivan1pl.ai4games.stats;

import java.util.Map;

public interface ContestStatistics {

    Map<String, PlayerStatistics> getStatistics();
    void print();

}

package com.ivan1pl.ai4games.stats;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RepeatedContestStatistics implements ContestStatistics {

    private static final String INDENT = "  ";

    private ArrayList<Map<String, PlayerStatistics>> statistics;

    public RepeatedContestStatistics() {
        this.statistics = new ArrayList<>();
    }

    @Override
    public Map<String, PlayerStatistics> getStatistics() {
        HashMap<String, PlayerStatistics> combinedStatistics = new HashMap<>();

        for (Map<String, PlayerStatistics> contestEntry : statistics) {
            for (Map.Entry<String, PlayerStatistics> entry : contestEntry.entrySet()) {
                PlayerStatistics playerStats = combinedStatistics.get(entry.getKey());

                if (playerStats == null) {
                    playerStats = new PlayerStatistics();
                    combinedStatistics.put(entry.getKey(), playerStats);
                }

                playerStats.wins += entry.getValue().wins;
                playerStats.loses += entry.getValue().loses;
                playerStats.ties += entry.getValue().ties;
            }
        }

        return combinedStatistics;
    }

    @Override
    public void print() {
        System.out.println("Overall repeated contest statistics:");

        Map<String, PlayerStatistics> combinedStats = getStatistics();
        for (Map.Entry<String, PlayerStatistics> entry : combinedStats.entrySet()) {
            System.out.println(INDENT + entry.getKey() + ": ");
            System.out.println(INDENT + INDENT + "wins:  " + entry.getValue().wins);
            System.out.println(INDENT + INDENT + "loses: " + entry.getValue().loses);
            System.out.println(INDENT + INDENT + "ties:  " + entry.getValue().ties);
        }
    }

    public List<Map<String, PlayerStatistics>> getStatisticsPerRound() {
        return statistics;
    }

    public void addNextRound(ContestStatistics stats) {
        statistics.add(stats.getStatistics());
    }

}

package com.ivan1pl.ai4games.stats;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.ivan1pl.ai4games.ipc.GameResponseMessage;
import com.ivan1pl.ai4games.ipc.PlayerAIStatusField;

public class GameStatistics {

    public static final GameStatistics EMPTY_STATISTICS = null;

    private final HashMap<String, PlayerAIGameResult> privateStatistics;
    private final String logFilePath;
    public final Map<String, PlayerAIGameResult> statistics;

    public GameStatistics(GameResponseMessage finalGameMessage, String logFilePath) {
        this.privateStatistics = new HashMap<>();
        this.logFilePath = logFilePath;

        for (PlayerAIStatusField playerAIStatus : finalGameMessage.players) {
            privateStatistics.put(playerAIStatus.id, PlayerAIGameResult.fromStatus(playerAIStatus.status));
        }

        this.statistics = Collections.unmodifiableMap(privateStatistics);
    }

    public void print(String gameName) {
        System.out.println("Game: " + gameName);

        for (Map.Entry<String, PlayerAIGameResult> entry : statistics.entrySet()) {
            System.out.println("  " + entry.getKey() + ": " + entry.getValue());
        }

        System.out.println("Replay: " + logFilePath);
        System.out.println();
    }

}

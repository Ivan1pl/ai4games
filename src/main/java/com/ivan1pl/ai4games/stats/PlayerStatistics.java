package com.ivan1pl.ai4games.stats;

public class PlayerStatistics {
    public int wins;
    public int loses;
    public int ties;

    public PlayerStatistics() {
        this.wins = 0;
        this.loses = 0;
        this.ties = 0;
    }
}

package com.ivan1pl.ai4games.stats;

import com.ivan1pl.ai4games.entity.PlayerAIStatus;

public enum PlayerAIGameResult {
    WIN,
    LOSE,
    TIE;
    
    public static PlayerAIGameResult fromStatus(PlayerAIStatus status) {
        switch (status) {
        case WON:
            return WIN;

        case LOST:
            return LOSE;

        case TIE:
            return TIE;

        default:
            return LOSE;
        }
    }
}

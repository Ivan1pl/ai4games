package com.ivan1pl.ai4games.stats;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class SingleContestStatistics implements ContestStatistics {

    private static final String INDENT = "  ";

    private String gameName;
    private HashMap<String, PlayerStatistics> statistics;

    public SingleContestStatistics(String gameName) {
        this.gameName = gameName;
        this.statistics = new HashMap<>();
    }

    @Override
    public void print() {
        System.out.println("Overall contest statistics for game: " + gameName);

        for (Map.Entry<String, PlayerStatistics> entry : statistics.entrySet()) {
            System.out.println(INDENT + entry.getKey() + ": ");
            System.out.println(INDENT + INDENT + "wins:  " + entry.getValue().wins);
            System.out.println(INDENT + INDENT + "loses: " + entry.getValue().loses);
            System.out.println(INDENT + INDENT + "ties:  " + entry.getValue().ties);
        }
    }

    @Override
    public Map<String, PlayerStatistics> getStatistics() {
        return Collections.unmodifiableMap(statistics);
    }

    public void addGameStats(GameStatistics gameStats) {
        if (gameStats == null) {
            return;
        }

        for (Map.Entry<String, PlayerAIGameResult> entry : gameStats.statistics.entrySet()) {
            PlayerStatistics playerStats = statistics.get(entry.getKey());

            if (playerStats == null) {
                playerStats = new PlayerStatistics();
                statistics.put(entry.getKey(), playerStats);
            }

            switch (entry.getValue()) {
            case WIN:
                playerStats.wins += 1;
                break;

            case LOSE:
                playerStats.loses += 1;
                break;

            case TIE:
                playerStats.ties += 1;
                break;
            }
        }
    }

}

package com.ivan1pl.ai4games.exceptions;

public class NotEnoughPlayersException extends Exception {

    public NotEnoughPlayersException(int playerCount, int minimalPlayerCount) {
        super("Not enough players detected!\nDetected: " + playerCount + "\nMinimal count: " + minimalPlayerCount);
    }

    public NotEnoughPlayersException(String message) {
        super(message);
    }

}

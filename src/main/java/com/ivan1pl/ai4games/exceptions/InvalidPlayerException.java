/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivan1pl.ai4games.exceptions;

/**
 *
 * @author Ivan1pl
 */
public class InvalidPlayerException extends Exception {
    
    public InvalidPlayerException(String message) {
        super(message);
    }
    
}

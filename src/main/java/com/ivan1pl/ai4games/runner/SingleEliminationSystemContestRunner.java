package com.ivan1pl.ai4games.runner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.ivan1pl.ai4games.entity.Game;
import com.ivan1pl.ai4games.entity.PlayerAI;
import com.ivan1pl.ai4games.stats.GameStatistics;
import com.ivan1pl.ai4games.stats.PlayerAIGameResult;

/*
 * This system works only for games when only 2 players are used for each play.
 */
public class SingleEliminationSystemContestRunner extends BaseContestRunner {

    private ArrayList<PlayerAI> currentRoundPlayers = null;
    private ArrayList<PlayerAI> nextRoundPlayers = null;

    public SingleEliminationSystemContestRunner(Game game, List<PlayerAI> players) {
        super(game, players);
    }

    @Override
    protected void resetInternalState() {
        Collections.shuffle(players);

        int pairedPlayerCount = getPairedCount();
        int unpairedPlayerCount = players.size() - pairedPlayerCount;

        currentRoundPlayers = new ArrayList<PlayerAI>(players.subList(0, 2 * unpairedPlayerCount));
        nextRoundPlayers = new ArrayList<PlayerAI>(players.subList(2 * unpairedPlayerCount, players.size()));
    }

    @Override
    protected List<PlayerAI> getNextGamePlayers(GameStatistics gameStats) {
        if (gameStats != null) {
            // Update head players according to their results
            // - win  > move to next round
            // - lose > remove from current round
            // - tie  > don't change his place (let him replay it)

            List<PlayerAI> lastPlayers = currentRoundPlayers.subList(0, 2);
            Iterator<PlayerAI> lastPlayersIterator = lastPlayers.iterator();

            while (lastPlayersIterator.hasNext()) {
                PlayerAI player = lastPlayersIterator.next();
                PlayerAIGameResult playerResult = gameStats.statistics.get(player.id);

                if (playerResult != PlayerAIGameResult.TIE) {
                    lastPlayersIterator.remove();

                    if (playerResult == PlayerAIGameResult.WIN) {
                        nextRoundPlayers.add(player);
                    }
                }
            }
        }

        if (currentRoundPlayers.isEmpty()) {
            // Round has ended, move to next
            assert(!nextRoundPlayers.isEmpty());
            currentRoundPlayers = nextRoundPlayers;
            nextRoundPlayers = new ArrayList<PlayerAI>();
        }

        if (currentRoundPlayers.size() == 1) {
            // All rounds have ended, only one player is left standing
            return null;
        }

        // Return first two remaining players from current round
        return new ArrayList<PlayerAI>(currentRoundPlayers.subList(0, 2));
    }

    private int getPairedCount() {
        // Biggest power of two that is smaller then players count
        int n = 2;
        while (n * 2 < players.size()) {
            n *= 2;
        }
        return n;
    }

}

package com.ivan1pl.ai4games.runner;

import java.security.InvalidParameterException;
import java.util.List;

import com.ivan1pl.ai4games.configuration.ExecutionSettings;
import com.ivan1pl.ai4games.entity.Game;
import com.ivan1pl.ai4games.entity.GameFactory;
import com.ivan1pl.ai4games.entity.PlayerAI;
import com.ivan1pl.ai4games.exceptions.InvalidPlayerException;
import com.ivan1pl.ai4games.exceptions.NotEnoughPlayersException;

public class ContestRunnerSelector {

    private GameFactory gameFactory;

    public ContestRunnerSelector(GameFactory gameFactory) {
        this.gameFactory = gameFactory;
    }

    public ContestRunner getContestRunner(ExecutionSettings settings)
            throws InvalidParameterException, InvalidPlayerException, NotEnoughPlayersException {
        ContestRunner result = null;

        Game game = gameFactory.getGame();
        List<PlayerAI> players = gameFactory.getPlayers();

        switch (settings.contestType) {
        case ROUND_ROBIN:
            result = new RoundRobinSystemContestRunner(game, players);
            break;

        case SINGLE_ELIMINATION:
            if (game.players != 2) {
                throw new InvalidParameterException("Single elimination system supports only games with 2 players!");
            }
            result = new SingleEliminationSystemContestRunner(game, players);
            break;

        case SWISS:
            result = new SwissSystemContestRunner(game, players);
            break;
        }

        if (settings.playCount > 1) {
            result = new RepeatingContestRunner(result, settings.playCount);
        }

        return result;
    }

}

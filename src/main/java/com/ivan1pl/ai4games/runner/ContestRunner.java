package com.ivan1pl.ai4games.runner;

import com.ivan1pl.ai4games.stats.ContestStatistics;


/*
 * Each ContestRunner shall NOT hold internal state outside of run() method.
 * This means that each ContestRunner must be able to process multiple calls 
 * to `run()` method and each time they should work independent of previous
 * state.
 */
public interface ContestRunner {
    ContestStatistics run();
}

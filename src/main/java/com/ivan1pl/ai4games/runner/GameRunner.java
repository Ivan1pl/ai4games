package com.ivan1pl.ai4games.runner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.google.common.collect.Iterators;
import com.ivan1pl.ai4games.entity.Game;
import com.ivan1pl.ai4games.entity.PlayerAI;
import com.ivan1pl.ai4games.entity.PlayerAIStatus;
import com.ivan1pl.ai4games.ipc.GameResponseMessage;
import com.ivan1pl.ai4games.ipc.PlayerAIResponseMessage;
import com.ivan1pl.ai4games.ipc.PlayerAIStatusField;
import com.ivan1pl.ai4games.loggers.GameplayLogger;
import com.ivan1pl.ai4games.stats.GameStatistics;

public class GameRunner {

    private final Game game;
    private final List<PlayerAI> players;
    private final Iterator<PlayerAI> playerIterator;
    private final GameplayLogger logger;

    public GameRunner(Game game, List<PlayerAI> players) {
        this.game = game;
        this.players = new ArrayList<PlayerAI>(players);
        this.playerIterator = Iterators.cycle(this.players);
        this.logger = new GameplayLogger(game.id, players.stream().map(p -> p.id).collect(Collectors.toList()));
    }

    public GameStatistics run() {
        GameStatistics stats = GameStatistics.EMPTY_STATISTICS;

        startGameProcess();
        startPlayerProcesses();

        try {
            game.sendMessage(PlayerAIResponseMessage.getInitialMessage(players), logger);

            while (true) {
                GameResponseMessage gameMessage = game.readMessage(logger);

                if (isEnd(gameMessage)) {
                    stats = getStatsFromGameMessage(gameMessage);
                    break;
                }

                PlayerAI player = getNextPlayer(gameMessage.players);
                player.sendMessage(gameMessage);

                PlayerAIResponseMessage playerMessage = player.readMessage(game.config.timeout, gameMessage);
                game.sendMessage(playerMessage, logger);

                if (!isPlayerAlive(player, playerMessage.players)) {
                    stopPlayerProcess(player);
                    playerIterator.remove();
                }
            }
        } catch (IOException exception) {
            Logger.getLogger(GameRunner.class.getName()).log(Level.SEVERE, exception.getMessage(), exception);
        }

        stopGameProcess();
        stopPlayerProcesses();

        logger.save();

        return stats;
    }

    private boolean isEnd(GameResponseMessage gameMessage) {
        for (PlayerAIStatusField playerStatus : gameMessage.players) {
            if (playerStatus.status == PlayerAIStatus.ALIVE) {
                return false;
            }
        }

        return true;
    }

    private GameStatistics getStatsFromGameMessage(GameResponseMessage gameMessage) {
        return new GameStatistics(gameMessage, logger.getFilePath());
    }

    private PlayerAI getNextPlayer(List<PlayerAIStatusField> playerStatuses) {
        PlayerAI nextPlayer = null;

        do {
            nextPlayer = playerIterator.next();
        } while (!isPlayerAlive(nextPlayer, playerStatuses));

        return nextPlayer;
    }

    private boolean isPlayerAlive(PlayerAI player, List<PlayerAIStatusField> playerStatuses) {
        for (PlayerAIStatusField playerStatus : playerStatuses) {
            if (playerStatus.id.equals(player.id) && playerStatus.status == PlayerAIStatus.ALIVE) {
                return true;
            }
        }

        return false;
    }

    private void startGameProcess() {
        game.process.start();
    }

    private void stopGameProcess() {
        game.process.kill();
    }

    private void startPlayerProcesses() {
        for (PlayerAI player : players) {
            player.process.start();
        }
    }

    private void stopPlayerProcess(PlayerAI player) {
        player.process.kill();
    }

    private void stopPlayerProcesses() {
        for (PlayerAI player : players) {
            stopPlayerProcess(player);
        }
    }

}

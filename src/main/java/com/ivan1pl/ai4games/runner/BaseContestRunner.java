package com.ivan1pl.ai4games.runner;

import java.util.ArrayList;
import java.util.List;

import com.ivan1pl.ai4games.entity.Game;
import com.ivan1pl.ai4games.entity.PlayerAI;
import com.ivan1pl.ai4games.stats.ContestStatistics;
import com.ivan1pl.ai4games.stats.GameStatistics;
import com.ivan1pl.ai4games.stats.SingleContestStatistics;

public abstract class BaseContestRunner implements ContestRunner {

    protected final Game game;
    protected final ArrayList<PlayerAI> players;

    public BaseContestRunner(Game game, List<PlayerAI> players) {
        this.game = game;
        this.players = new ArrayList<>(players);
    }

    @Override
    public ContestStatistics run() {
        resetInternalState();

        SingleContestStatistics contestStats = new SingleContestStatistics(game.id);
        List<PlayerAI> gamePlayers = getNextGamePlayers(null);

        while (gamePlayers != null) {
            GameRunner gameRunner = new GameRunner(game, gamePlayers);
            GameStatistics gameStats = gameRunner.run();

            gameStats.print(game.id);
            contestStats.addGameStats(gameStats);

            gamePlayers = getNextGamePlayers(gameStats);
        }

        return contestStats;
    }

    protected abstract void resetInternalState();
    protected abstract List<PlayerAI> getNextGamePlayers(GameStatistics gameStats);

}

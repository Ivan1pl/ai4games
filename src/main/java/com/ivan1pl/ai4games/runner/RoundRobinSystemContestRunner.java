package com.ivan1pl.ai4games.runner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;
import com.ivan1pl.ai4games.entity.Game;
import com.ivan1pl.ai4games.entity.PlayerAI;
import com.ivan1pl.ai4games.stats.GameStatistics;

public class RoundRobinSystemContestRunner extends BaseContestRunner {

    Set<List<PlayerAI>> generatedPlayerTuples;
    Iterator<List<PlayerAI>> generatedPlayerTuplesIterator;

    public RoundRobinSystemContestRunner(Game game, List<PlayerAI> players) {
        super(game, players);

        generatedPlayerTuples = null;
        generatedPlayerTuplesIterator = null;
    }

    @Override
    protected void resetInternalState() {
        generatedPlayerTuples = null;
        generatedPlayerTuplesIterator = null;
    }

    @Override
    protected List<PlayerAI> getNextGamePlayers(GameStatistics gameStats) {
        if (generatedPlayerTuples == null) {
            generatePlayerTuples();
        }

        List<PlayerAI> playerList = null;
        while (generatedPlayerTuplesIterator.hasNext()) {
            List<PlayerAI> currentPlayerList = generatedPlayerTuplesIterator.next();

            if (isPlayerListValid(currentPlayerList)) {
                playerList = currentPlayerList;
                break;
            }
        }

        return playerList;
    }

    private void generatePlayerTuples() {
        List<Set<PlayerAI>> nListsOfPlayers = new ArrayList<>();
        for (int i = 0; i < game.players; ++i) {
            nListsOfPlayers.add(new HashSet<PlayerAI>(players));
        }

        generatedPlayerTuples = Sets.cartesianProduct(nListsOfPlayers);
        generatedPlayerTuplesIterator = generatedPlayerTuples.iterator();
    }

    private boolean isPlayerListValid(List<PlayerAI> playerList) {
        // We don't allow duplicates, so we need to make sure that there are none!
        Set<PlayerAI> playerSet = new HashSet<>(playerList);
        return (playerList.size() == playerSet.size());
    }

}

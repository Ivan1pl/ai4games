package com.ivan1pl.ai4games.runner;

import com.ivan1pl.ai4games.stats.ContestStatistics;
import com.ivan1pl.ai4games.stats.RepeatedContestStatistics;

public class RepeatingContestRunner implements ContestRunner {

    private ContestRunner internalRunner;
    private int repeatCount;

    public RepeatingContestRunner(ContestRunner internalRunner, int repeatCount) {
        this.internalRunner = internalRunner;
        this.repeatCount = repeatCount;
    }

    @Override
    public ContestStatistics run() {
        RepeatedContestStatistics stats = new RepeatedContestStatistics();

        for (int i = 0; i < repeatCount; ++i) {
            ContestStatistics currentStats = internalRunner.run();

            currentStats.print();
            stats.addNextRound(currentStats);
        }

        return stats;
    }

}

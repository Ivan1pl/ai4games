/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivan1pl.ai4games.loggers;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 *
 * @author Ivan1pl
 */
public class GameplayLogger extends FileLogger {

    private static int games = 0;
    private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss");
    private static final LocalDateTime now = LocalDateTime.now();

    public GameplayLogger(String gameName, List<String> playerNames) {
        super(new File(prepareOutputFilePath(gameName), "" + (++games) + playerNamesToString(playerNames) + ".log"));
    }

    private static String prepareOutputFilePath(String gameName) {
        File outputParent = new File("games" + File.separator + gameName + File.separator + "logs" + File.separator + dtf.format(now));
        outputParent.mkdirs();
        return outputParent.getAbsolutePath();
    }

    private static String playerNamesToString(List<String> playerNames) {
        String result = "";
        for (String s : playerNames) {
            result += "_" + s;
        }
        return result;
    }

}

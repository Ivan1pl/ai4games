/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivan1pl.ai4games.loggers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.logging.Level;

/**
 *
 * @author Ivan1pl
 */
public class FileLogger implements Logger {
    private final PrintWriter writer;
    private final String path;
    
    public FileLogger(File outputFile) {
        this.path = outputFile.getAbsolutePath();
        File parent = outputFile.getParentFile();
        parent.mkdirs();
        PrintWriter w = null;
        try {
            w = new PrintWriter(outputFile);
        } catch (FileNotFoundException ex) {
            java.util.logging.Logger.getLogger(FileLogger.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.writer = w;
    }

    @Override
    public void log(String message) {
        if (writer != null) {
            writer.println(message);
        }
    }
    
    public void save() {
        writer.close();
    }
    
    public String getFilePath() {
        return path;
    }
    
}
